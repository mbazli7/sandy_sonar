#!/usr/bin/python3

"""
This is the GUI program to design path planning.
"""

import sys
import pygame
import math
import json
from enum import IntEnum

"""
79px ~= 620cm
MAP_SIZE_COEFF = 620/79
"""
MAP_SIZE_COEFF = 7.85
MAP_IMAGE = 'map/image.png'

WINDOW_WIDTH = 1080
WINDOW_HEIGHT = 720
BLOCK_SIZE_CM = 72  # adjust to match actual map.

COLOR_GREY = (224, 224, 224)
COLOR_GREEN = (0, 186, 43)
COLOR_RED = (255, 0, 0)
COLOR_BLACK = (0, 0, 0)
COLOR_WHITE = (255, 255, 255)


# Initialize pygame window.
pygame.init()
pygame.display.set_caption('PATH PLANNING UI')
screen = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
btn_state = {}

# Save to filename.
fname = 'map/{}'.format(sys.argv[1])


class DroneType(IntEnum):
    TELLO = 0
    PX4 = 1


class Direction(IntEnum):
    LEFT = 0
    RIGHT = 1
    STRAIGHT = 2


class Shape(IntEnum):
    LINE = 0
    CIRCLE = 1


class Speed(IntEnum):
    SPEED_MIN_CM_S = 10
    SPEED_MED_CM_S = 50
    SPEED_MAX_CM_S = 100


class Background(pygame.sprite.Sprite):
    """
    Load image on pygame background.
    """

    def __init__(self, image, location, scale):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(image)
        self.image = pygame.transform.rotozoom(self.image, 0, scale)
        self.rect = self.image.get_rect()
        self.rect.left, self.rect.top = location


def draw_grid(color, blocksize):
    """
    Draw block grid for reference.
    """
    for x in range(WINDOW_WIDTH):
        for y in range(WINDOW_HEIGHT):
            rect = pygame.Rect((x * blocksize), (y * blocksize), blocksize, blocksize)
            pygame.draw.rect(screen, color, rect, 1)
    pygame.display.update()


def draw_button(text, text_color, bgcolor, pos, width, height):
    """
    Draw button with text.
    """
    # Set font.
    font = pygame.font.SysFont('Arial', 16)

    # Keep track button position.
    btn_state.update({text: {"pos": pos}})

    # Rect() argument => (left, top, width, height)
    button = pygame.Rect(pos[0], pos[1], width, height)
    button = pygame.draw.rect(screen, bgcolor, button)
    text = font.render(text, True, text_color)
    text_coord = (pos[0] + width/2), (pos[1] + height/2)
    text_rect = text.get_rect(center=text_coord)

    # Update screen.
    screen.blit(text, text_rect)
    pygame.display.update()
    return button


def get_dist_btw_pos(pos0, pos1):
    """
    Get distance between 2 mouse position.
    """
    x = abs(pos0[0] - pos1[0])
    y = abs(pos0[1] - pos1[1])
    dist_px = math.hypot(x, y)
    dist_cm = dist_px * MAP_SIZE_COEFF
    return int(dist_cm), int(dist_px)


def get_angle_btw_line(pos0, pos1, posref):
    """
    Compute angle between two vector coordinates with reference point.
    TODO: get angle direction so that button-left/right/straight can be removed.
    """
    ax = posref[0] - pos0[0]
    ay = posref[1] - pos0[1]
    bx = posref[0] - pos1[0]
    by = posref[1] - pos1[1]
    # Get dot product of pos0 and pos1.
    _dot = (ax * bx) + (ay * by)
    # Get magnitude of pos0 and pos1.
    _mag_a = math.sqrt(ax**2 + ay**2)
    _mag_b = math.sqrt(bx**2 + by**2)
    _rad = math.acos(_dot / (_mag_a * _mag_b))
    # Angle in degrees.
    angle = (_rad * 180) / math.pi
    return int(angle)


def compute_waypoints(path_wp):
    """
    Generate waypoints.
    """
    path_dist_cm = []
    path_dist_px = []
    path_angle = []
    for index in range(len(path_wp)):
        if index > 1:
            # Skip the first and second index.
            dist_cm, dist_px = get_dist_btw_pos(path_wp[index-1], path_wp[index])
            path_dist_cm.append(dist_cm)
            path_dist_px.append(dist_px)
        elif index > 0 and index < (len(path_wp) - 1):
            # Skip the first and last index.
            angle = get_angle_btw_line(path_wp[index-1], path_wp[index+1], path_wp[index])
            path_angle.append(angle)

    """
    Remove first index if length not matched.
    This is need when a JSON file is loaded without any new mouse click point.
    """
    if len(path_dir) > len(path_dist_cm):
        path_dir.pop(0)
        path_speed.pop(0)

    # Save waypoints into JSON file.
    waypoints = []
    for index in range(len(path_dist_cm)):
        # Get outer angle instead of acute for every corner.
        angle_deg = path_angle[index]
        if index > 0 and path_dir[index] != 2:
            angle_deg = 180 - path_angle[index]
            path_angle[index] = angle_deg  # Replace angle.

        if path_dist_cm[index] > 500:
            # Append original waypoints but limit to 500cm.
            waypoints.append({
                "dir": path_dir[index],
                "dist_cm": 500,
                "dist_px": int(500 * 79 / 620),  # Update pixel when change map/scale.
                "angle_deg": angle_deg,
                "speed_cm_s": path_speed[index]
            })

            # Calculate balance forward loop times.
            bal_forward_loop = int((path_dist_cm[index] - 500) / 500)
            for _ in range(bal_forward_loop):
                waypoints.append({
                    "dir": Direction.STRAIGHT,
                    "dist_cm": 500,
                    "dist_px": int(500 * 79 / 620),  # Update pixel when change map/scale.
                    "angle_deg": 0,
                    "speed_cm_s": path_speed[index]
                })

            # Append remainder forward distance.
            bal_forward_dist_cm = int((path_dist_cm[index] - 500) % 500)
            waypoints.append({
                "dir": Direction.STRAIGHT,
                "dist_cm": bal_forward_dist_cm,
                "dist_px": int(bal_forward_dist_cm * 79 / 620),  # Update pixel when change map/scale.
                "angle_deg": 0,
                "speed_cm_s": path_speed[index]
            })
            
            # Back to top of loop
            continue

        # Directly append waypoints if less than or equal 500
        waypoints.append({
            "dir": path_dir[index],
            "dist_cm": path_dist_cm[index],
            "dist_px": path_dist_px[index],
            "angle_deg": angle_deg,
            "speed_cm_s": path_speed[index]
        })

    # Print out the information.
    print('\npath_dir: {}'.format(path_dir))
    print('dist_cm: {}'.format(path_dist_cm))
    print('dist_px: {}'.format(path_dist_px))
    print('angle_deg: {}'.format(path_angle))
    print('path_speed_cm_s: {}'.format(path_speed))
    print('path_wp: {}'.format(path_wp))

    # Save to JSON file.
    f = open(fname, 'w+')
    path_wp.pop(0)
    json.dump({
        "wp": waypoints,
        "coords": path_wp
    }, f, indent=4)
    f.close()


def init_window():
    """
    Initialize window's component.
    """
    # Fill background color.
    screen.fill(COLOR_WHITE)
    pygame.mouse.set_cursor(*pygame.cursors.broken_x)

    # Load background image.
    background = Background(MAP_IMAGE, [120, 20], 1.8)
    screen.blit(background.image, background.rect)

    # Draw grid.
    draw_grid(COLOR_WHITE, 20)

    # Draw buttons.
    btn_obj = {
        # Shape
        'Line': draw_button('Line', COLOR_BLACK, COLOR_GREEN, (5, 5), 100, 40),
        'Circle': draw_button('Circle', COLOR_BLACK, COLOR_GREY, (5, 50), 100, 40),
        # Direction
        'Left': draw_button('Left', COLOR_BLACK, COLOR_GREY, (5, 105), 100, 40),
        'Right': draw_button('Right', COLOR_BLACK, COLOR_GREY, (5, 150), 100, 40),
        'Straight': draw_button('Straight', COLOR_BLACK, COLOR_GREEN, (5, 195), 100, 40),
        # Speed
        'SpeedMin': draw_button('SpeedMin', COLOR_BLACK, COLOR_GREY, (5, 250), 100, 40),
        'SpeedMed': draw_button('SpeedMed', COLOR_BLACK, COLOR_GREY, (5, 295), 100, 40),
        'SpeedMax': draw_button('SpeedMax', COLOR_BLACK, COLOR_GREEN, (5, 340), 100, 40),
        # Action
        'Reset': draw_button('Reset', COLOR_BLACK, COLOR_GREY, (5, 395), 100, 40),
        'Complete': draw_button('Complete', COLOR_BLACK, COLOR_GREY, (5, 440), 100, 40)
    }

    # Update display.
    pygame.display.update()
    return btn_obj


# Initialize variables.
running = True
btn_obj = init_window()
drone_name = DroneType.TELLO
draw_mode = Shape.LINE
direction = Direction.STRAIGHT
speed = Speed.SPEED_MAX_CM_S
path_wp = []
path_dir = []
path_dist_cm = []
path_dist_px = []
path_angle = []
path_speed = []
index = 0


"""
Load and preview existing waypoints from JSON file.
"""
try:
    f = open(fname,)
    waypoints = json.load(f)
    f.close()

    for index in range(len(waypoints['coords'])):
        if index > 0:  # Skip first index.
            pygame.draw.line(screen, COLOR_RED, waypoints['coords'][index-1], waypoints['coords'][index], 2)
            pygame.display.update()

    for index in range(len(waypoints['wp'])):
        # Restore waypoints for cont. development.
        path_dir.append(waypoints['wp'][index]['dir'])
        path_speed.append(waypoints['wp'][index]['speed_cm_s'])

    # Restore pos coordinates.
    path_wp = waypoints['coords']
except FileNotFoundError:
    pass


"""
Main path planning program.
"""
while running:
    # Get mouse position.
    pos = pygame.mouse.get_pos()

    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONDOWN:
            """
            Button shape.
            """
            if btn_obj['Line'].collidepoint(pos):
                init_window()
                btn_obj['Line'] = draw_button('Line', COLOR_BLACK, COLOR_GREEN, btn_state['Line']['pos'], 100, 40)
                btn_obj['Circle'] = draw_button('Circle', COLOR_BLACK, COLOR_GREY, btn_state['Circle']['pos'], 100, 40)
                # Reset.
                draw_mode = Shape.LINE
                direction = Direction.STRAIGHT
                speed = Speed.SPEED_MAX_CM_S
                path_wp = []
                path_dir = []
                path_dist_cm = []
                path_dist_px = []
                path_angle = []
                path_speed = []
                index = 0
            elif btn_obj['Circle'].collidepoint(pos):
                init_window()
                btn_obj['Line'] = draw_button('Line', COLOR_BLACK, COLOR_GREY, btn_state['Line']['pos'], 100, 40)
                btn_obj['Circle'] = draw_button('Circle', COLOR_BLACK, COLOR_GREEN, btn_state['Circle']['pos'], 100, 40)
                # Reset.
                draw_mode = Shape.CIRCLE
                direction = Direction.STRAIGHT
                speed = Speed.SPEED_MAX_CM_S
                path_wp = []
                path_dir = []
                path_dist_cm = []
                path_dist_px = []
                path_angle = []
                path_speed = []
                index = 0

            """
            Button direction.
            """
            if btn_obj['Left'].collidepoint(pos):
                btn_obj['Left'] = draw_button('Left', COLOR_BLACK, COLOR_GREEN, btn_state['Left']['pos'], 100, 40)
                btn_obj['Right'] = draw_button('Right', COLOR_BLACK, COLOR_GREY, btn_state['Right']['pos'], 100, 40)
                btn_obj['Straight'] = draw_button('Straight', COLOR_BLACK, COLOR_GREY,
                                                  btn_state['Straight']['pos'], 100, 40)
                direction = Direction.LEFT
            elif btn_obj['Right'].collidepoint(pos):
                btn_obj['Left'] = draw_button('Left', COLOR_BLACK, COLOR_GREY, btn_state['Left']['pos'], 100, 40)
                btn_obj['Right'] = draw_button('Right', COLOR_BLACK, COLOR_GREEN, btn_state['Right']['pos'], 100, 40)
                btn_obj['Straight'] = draw_button('Straight', COLOR_BLACK, COLOR_GREY,
                                                  btn_state['Straight']['pos'], 100, 40)
                direction = Direction.RIGHT
            elif btn_obj['Straight'].collidepoint(pos):
                btn_obj['Left'] = draw_button('Left', COLOR_BLACK, COLOR_GREY, btn_state['Left']['pos'], 100, 40)
                btn_obj['Right'] = draw_button('Right', COLOR_BLACK, COLOR_GREY, btn_state['Right']['pos'], 100, 40)
                btn_obj['Straight'] = draw_button('Straight', COLOR_BLACK, COLOR_GREEN,
                                                  btn_state['Straight']['pos'], 100, 40)
                direction = Direction.STRAIGHT

            """
            Button speed.
            """
            if btn_obj['SpeedMin'].collidepoint(pos):
                btn_obj['SpeedMin'] = draw_button('SpeedMin', COLOR_BLACK, COLOR_GREEN,
                                                  btn_state['SpeedMin']['pos'], 100, 40)
                btn_obj['SpeedMed'] = draw_button('SpeedMed', COLOR_BLACK, COLOR_GREY,
                                                  btn_state['SpeedMed']['pos'], 100, 40)
                btn_obj['SpeedMax'] = draw_button('SpeedMax', COLOR_BLACK, COLOR_GREY,
                                                  btn_state['SpeedMax']['pos'], 100, 40)
                speed = Speed.SPEED_MIN_CM_S
            elif btn_obj['SpeedMed'].collidepoint(pos):
                btn_obj['SpeedMin'] = draw_button('SpeedMin', COLOR_BLACK, COLOR_GREY,
                                                  btn_state['SpeedMin']['pos'], 100, 40)
                btn_obj['SpeedMed'] = draw_button('SpeedMed', COLOR_BLACK, COLOR_GREEN,
                                                  btn_state['SpeedMed']['pos'], 100, 40)
                btn_obj['SpeedMax'] = draw_button('SpeedMax', COLOR_BLACK, COLOR_GREY,
                                                  btn_state['SpeedMax']['pos'], 100, 40)
                speed = Speed.SPEED_MED_CM_S
            elif btn_obj['SpeedMax'].collidepoint(pos):
                btn_obj['SpeedMin'] = draw_button('SpeedMin', COLOR_BLACK, COLOR_GREY,
                                                  btn_state['SpeedMin']['pos'], 100, 40)
                btn_obj['SpeedMed'] = draw_button('SpeedMed', COLOR_BLACK, COLOR_GREY,
                                                  btn_state['SpeedMed']['pos'], 100, 40)
                btn_obj['SpeedMax'] = draw_button('SpeedMax', COLOR_BLACK, COLOR_GREEN,
                                                  btn_state['SpeedMax']['pos'], 100, 40)
                speed = Speed.SPEED_MAX_CM_S

            """
            Button reset.
            """
            if btn_obj['Reset'].collidepoint(pos):
                # Clear button formatting.
                for key in btn_obj.keys():
                    btn_obj[key] = draw_button(key, COLOR_BLACK, COLOR_GREY, btn_state[key]['pos'], 100, 40)
                # Init window.
                init_window()
                # Reset.
                draw_mode = Shape.LINE
                direction = Direction.STRAIGHT
                speed = Speed.SPEED_MAX_CM_S
                path_wp = []
                path_dir = []
                path_dist_cm = []
                path_dist_px = []
                path_angle = []
                path_speed = []
                index = 0

            """
            Button complete.
            """
            if btn_obj['Complete'].collidepoint(pos):
                # Set dummy initial point.
                if draw_mode is Shape.LINE and index > 0:
                    path_wp.insert(0, (path_wp[0][0], abs(path_wp[0][1] - 10)))
                # Compute waypoints.
                compute_waypoints(path_wp)

            # Mouse click outside any buttons.
            if not any(btn_obj[key].collidepoint(pos) for key in btn_obj.keys()):
                # Skip first index.
                if index > 0 and draw_mode == Shape.LINE:
                    pygame.draw.line(screen, COLOR_RED, path_wp[-1], pos, 2)

                # Update display.
                pygame.draw.circle(screen, COLOR_RED, pos, 3, 1)  # Pos marker.
                pygame.display.update()

                # Update waypoints
                path_wp.append(pos)
                path_dir.append(direction)
                path_speed.append(speed)
                index += 1
        elif event.type == pygame.QUIT:
            running = False
